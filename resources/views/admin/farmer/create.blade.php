@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.farmer.title_singular') }}
    </div>

    <div class="card-body">
        <form action="" method="POST" enctype="multipart/form-data">
            @csrf
            <div class="row ">
                <div class="row col-md-6 col-xs-12"  >
                    <div class="form-group col-sm-4{{ $errors->has('farmer_name') ? 'has-error' : '' }}">
                        <label for="farmer_name">{{ trans('cruds.farmer.fields.farmer_name') }}*</label>
                        <input type="text" id="farmer_name" name="farmer_name" class="form-control"
                            value="{{ old('farmer_name', isset($farmer) ? $farmer->farmer_name : '') }}" required>
                        @if($errors->has('farmer_name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('farmer_name') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.farmer_name_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4{{ $errors->has('dob') ? 'has-error' : '' }}">
                        <label for="dob">{{ trans('cruds.farmer.fields.dob') }}*</label>
                        <input type="date" id="dob" name="dob" class="form-control"
                            value="{{ old('dob', isset($farmer) ? $farmer->dob : '') }}" required>
                        @if($errors->has('dob'))
                        <em class="invalid-feedback">
                            {{ $errors->first('dob') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.dob_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label for="email">{{ trans('cruds.farmer.fields.email') }}*</label>
                        <input type="email" id="email" name="email" class="form-control" required>
                        @if($errors->has('email'))
                        <em class="invalid-feedback">
                            {{ $errors->first('email') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.email_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4{{ $errors->has('address') ? 'has-error' : '' }}">
                        <label for="address">{{ trans('cruds.farmer.fields.address') }}*</label>
                        <input type="address" id="address" name="address" class="form-control"
                            value="{{ old('address', isset($farmer) ? $farmer->address : '') }}" required>
                        @if($errors->has('address'))
                        <em class="invalid-feedback">
                            {{ $errors->first('address') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.address_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('contact_no') ? 'has-error' : '' }}">
                        <label for="contact_no">{{ trans('cruds.farmer.fields.contact_no') }}*</label>
                        <input type="text" id="contact_no" name="contact_no" class="form-control" required>
                        @if($errors->has('contact_no'))
                        <em class="invalid-feedback">
                            {{ $errors->first('contact_no') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.contact_no_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('block') ? 'has-error' : '' }}">
                        <label for="block">{{ trans('cruds.farmer.fields.block') }}*</label>
                        <input type="text" id="block" name="block" class="form-control" required>
                        @if($errors->has('block'))
                        <em class="invalid-feedback">
                            {{ $errors->first('block') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.block_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('village') ? 'has-error' : '' }}">
                        <label for="village">{{ trans('cruds.farmer.fields.village') }}*</label>
                        <input type="text" id="taluka" name="village" class="form-control" required>
                        @if($errors->has('village'))
                        <em class="invalid-feedback">
                            {{ $errors->first('village') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.village_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('city') ? 'has-error' : '' }}">
                        <label for="city">{{ trans('cruds.farmer.fields.city') }}*</label>
                        <input type="text" id="city" name="city" class="form-control" required>
                        @if($errors->has('city'))
                        <em class="invalid-feedback">
                            {{ $errors->first('city') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.city_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('district') ? 'has-error' : '' }}">
                        <label for="district">{{ trans('cruds.farmer.fields.district') }}*</label>
                        <input type="text" id="district" name="district" class="form-control" required>
                        @if($errors->has('district'))
                        <em class="invalid-feedback">
                            {{ $errors->first('district') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.district_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('state') ? 'has-error' : '' }}">
                        <label for="state">{{ trans('cruds.farmer.fields.state') }}*</label>
                        <input type="text" id="state" name="state" class="form-control" required>
                        @if($errors->has('state'))
                        <em class="invalid-feedback">
                            {{ $errors->first('state') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.state_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('irrigated_area') ? 'has-error' : '' }}">
                        <label for="irrigated_area">{{ trans('cruds.farmer.fields.irrigated_area') }}*</label>
                        <input type="text" id="irrigated_area" name="irrigated_area" class="form-control" required>
                        @if($errors->has('irrigated_area'))
                        <em class="invalid-feedback">
                            {{ $errors->first('irrigated_area') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.irrigated_area_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('unirrigated_area') ? 'has-error' : '' }}">
                        <label for="unirrigated_area">{{ trans('cruds.farmer.fields.unirrigated_area') }}*</label>
                        <input type="text" id="unirrigated_area" name="unirrigated_area" class="form-control" required>
                        @if($errors->has('unirrigated_area'))
                        <em class="invalid-feedback">
                            {{ $errors->first('unirrigated_area') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.unirrigated_area_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('infrastructure_detail') ? 'has-error' : '' }}">
                        <label for="infrastructure_detail">{{ trans('cruds.farmer.fields.infrastructure_detail')
                            }}*</label>
                        <input type="text" id="infrastructure_detail" name="infrastructure_detail" class="form-control"
                            required>
                        @if($errors->has('infrastructure_detail'))
                        <em class="invalid-feedback">
                            {{ $errors->first('infrastructure_detail') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.infrastructure_detail_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('vegetables_grown') ? 'has-error' : '' }}">
                        <label for="vegetables_grown">{{ trans('cruds.farmer.fields.vegetables_grown') }}*</label>
                        <input type="text" id="vegetables_grown" name="vegetables_grown" class="form-control" required>
                        @if($errors->has('vegetables_grown'))
                        <em class="invalid-feedback">
                            {{ $errors->first('vegetables_grown') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.vegetables_grown_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('aadhar_number') ? 'has-error' : '' }}">
                        <label for="aadhar_number">{{ trans('cruds.farmer.fields.aadhar_number') }}*</label>
                        <input type="text" id="aadhar_number" name="aadhar_number" class="form-control" required>
                        @if($errors->has('aadhar_number'))
                        <em class="invalid-feedback">
                            {{ $errors->first('aadhar_number') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.aadhar_number_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('kharsa_no') ? 'has-error' : '' }}">
                        <label for="kharsa_no">{{ trans('cruds.farmer.fields.kharsa_no') }}*</label>
                        <input type="text" id="kharsa_no" name="kharsa_no" class="form-control" required>
                        @if($errors->has('kharsa_no'))
                        <em class="invalid-feedback">
                            {{ $errors->first('kharsa_no') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.kharsa_no_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('irrigation') ? 'has-error' : '' }}">
                        <label for="irrigation">{{ trans('cruds.farmer.fields.irrigation') }}*</label>
                        <input type="text" id="irrigation" name="irrigation" class="form-control" required>
                        @if($errors->has('irrigation'))
                        <em class="invalid-feedback">
                            {{ $errors->first('irrigation') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.irrigation_helper') }}
                        </p>

                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('water_source') ? 'has-error' : '' }}">

                        <label>{{ trans('cruds.farmer.fields.water_source') }} </label>
                        <select class="form-control">
                            <option value=" ">Select Water Source</option>
                            <option value="borewell"> Bore Well</option>
                            <option value="openwell"> Open Well</option>
                            <option value="river">River</option>
                            <option value="canal">Canal</option>
                            <option value="tank">Tank</option>
                            <option value="pond">Pond</option>
                            <option value="checkdam">Check Dam</option>
                            <option value="drip">Drip</option>
                            <option value="other">Other</option>
                            <option value="none">None</option>

                        </select>
                        @if($errors->has('water_source'))
                        <em class="invalid-feedback">
                            {{ $errors->first('water_source') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.water_source_helper') }}
                        </p>




                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('transport') ? 'has-error' : '' }}">
                        <label for="transport">{{ trans('cruds.farmer.fields.transport') }}*</label>
                        <input type="text" id="transport" name="transport" class="form-control" required>
                        @if($errors->has('transport'))
                        <em class="invalid-feedback">
                            {{ $errors->first('transport') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.transport_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('seed_type') ? 'has-error' : '' }}">
                        <label for="seed_type">{{ trans('cruds.farmer.fields.seed_type') }}*</label>
                        <input type="text" id="seed_type" name="seed_type" class="form-control" required>
                        @if($errors->has('seed_type'))
                        <em class="invalid-feedback">
                            {{ $errors->first('seed_type') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.seed_type_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('seed_buy_from') ? 'has-error' : '' }}">
                        <label for="seed_buy_from">{{ trans('cruds.farmer.fields.seed_type') }}*</label>
                        <input type="text" id="seed_buy_from" name="seed_buy_from" class="form-control" required>
                        @if($errors->has('seed_buy_from'))
                        <em class="invalid-feedback">
                            {{ $errors->first('seed_buy_from') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.seed_buy_from_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('seed_buy_from') ? 'has-error' : '' }}">
                        <label for="seed_buy_from">{{ trans('cruds.farmer.fields.seed_type') }}*</label>
                        <input type="text" id="seed_buy_from" name="seed_buy_from" class="form-control" required>
                        @if($errors->has('seed_buy_from'))
                        <em class="invalid-feedback">
                            {{ $errors->first('seed_buy_from') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.seed_buy_from_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('dependents') ? 'has-error' : '' }}">
                        <label for="dependents">{{ trans('cruds.farmer.fields.dependents') }}*</label>
                        <input type="text" id="dependents" name="dependents" class="form-control" required>
                        @if($errors->has('dependents'))
                        <em class="invalid-feedback">
                            {{ $errors->first('dependents') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.dependents_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('land_organic_farming') ? 'has-error' : '' }}">
                        <label for="land_organic_farming">{{ trans('cruds.farmer.fields.land_organic_farming')
                            }}*</label>
                        <input type="text" id="land_organic_farming" name="land_organic_farming" class="form-control"
                            required>
                        @if($errors->has('land_organic_farming'))
                        <em class="invalid-feedback">
                            {{ $errors->first('land_organic_farming') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.land_organic_farming_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('manure') ? 'has-error' : '' }}">
                        <label for="manure">{{ trans('cruds.farmer.fields.manure') }}*</label>
                        <input type="text" id="manure" name="manure" class="form-control" required>
                        @if($errors->has('manure'))
                        <em class="invalid-feedback">
                            {{ $errors->first('manure') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.manure_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('fertilizer_type') ? 'has-error' : '' }}">
                        <label for="fertilizer_type">{{ trans('cruds.farmer.fields.fertilizer_type') }}*</label>
                        <input type="text" id="fertilizer_type" name="fertilizer_type" class="form-control" required>
                        @if($errors->has('fertilizer_type'))
                        <em class="invalid-feedback">
                            {{ $errors->first('fertilizer_type') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.fertilizer_type_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('fertilizer_obtain_from') ? 'has-error' : '' }}">
                        <label for="fertilizer_obtain_from">{{ trans('cruds.farmer.fields.fertilizer_obtain_from')
                            }}*</label>
                        <input type="text" id="fertilizer_obtain_from" name="fertilizer_obtain_from"
                            class="form-control" required>
                        @if($errors->has('fertilizer_obtain_from'))
                        <em class="invalid-feedback">
                            {{ $errors->first('fertilizer_obtain_from') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.fertilizer_obtain_from_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('pesticides_used') ? 'has-error' : '' }}">
                        <label for="pesticides_used">{{ trans('cruds.farmer.fields.pesticides_used') }}*</label>
                        <input type="text" id="pesticides_used" name="pesticides_used" class="form-control" required>
                        @if($errors->has('pesticides_used'))
                        <em class="invalid-feedback">
                            {{ $errors->first('pesticides_used') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.pesticides_used_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('cost_of_production') ? 'has-error' : '' }}">
                        <label for="cost_of_production">{{ trans('cruds.farmer.fields.cost_of_production') }}*</label>
                        <input type="text" id="cost_of_production" name="cost_of_production" class="form-control"
                            required>
                        @if($errors->has('cost_of_production'))
                        <em class="invalid-feedback">
                            {{ $errors->first('cost_of_production') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.cost_of_production_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('father_name') ? 'has-error' : '' }}">
                        <label for="father_name">{{ trans('cruds.farmer.fields.father_name') }}*</label>
                        <input type="text" id="father_name" name="father_name" class="form-control" required>
                        @if($errors->has('father_name'))
                        <em class="invalid-feedback">
                            {{ $errors->first('father_name') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.father_name_helper') }}
                        </p>
                    </div>
                    <div class="form-group col-sm-4 {{ $errors->has('period') ? 'has-error' : '' }}">
                        <label for="period">{{ trans('cruds.farmer.fields.period') }}*</label>
                        <input type="text" id="period" name="period" class="form-control" required>
                        @if($errors->has('period'))
                        <em class="invalid-feedback">
                            {{ $errors->first('period') }}
                        </em>
                        @endif
                        <p class="helper-block">
                            {{ trans('cruds.farmer.fields.period_helper') }}
                        </p>
                    </div>
                </div>
                <div class="row col-md-6 col-xs-12">
                    <div class="row border rounded h-35  pt-2 ml-2">
                        <div class="form-group col {{ $errors->has('caste') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.caste') }}
                            <div class="d-flex  col form-control mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="general" type="radio" name="caste">
                                    <label class="form-check-label" for="general">General</label>
                                    @if($errors->has('caste'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('caste') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.caste_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="sc" type="radio" name="caste">
                                    <label class="form-check-label" for="sc">SC</label>
                                    @if($errors->has('caste'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('caste') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.caste_helper') }}
                                    </p>
                                </div>
                                <div class="form-check d-flex">
                                    <input class="form-check-input" id="obc" type="radio" name="caste">
                                    <label class="form-check-label" for="obc">OBC</label>
                                    @if($errors->has('caste'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('caste') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.caste_helper') }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col {{ $errors->has('farmer_type') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.farmer_type') }}
                            <div class="d-flex col  p-2 mt-2 form-control">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="small" type="radio" name="farmer_type">
                                    <label class="form-check-label" for="small">Small</label>
                                    @if($errors->has('farmer_type'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('farmer_type') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.farmer_type_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="medium" type="radio" name="farmer_type">
                                    <label class="form-check-label" for="medium">Medium</label>
                                    @if($errors->has('farmer_type'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('farmer_type') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.farmer_type_helper') }}
                                    </p>
                                </div>
                                <div class="form-check d-flex">
                                    <input class="form-check-input" id="large" type="radio" name="farmer_type">
                                    <label class="form-check-label" for="large">Large</label>
                                    @if($errors->has('farmer_type'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('farmer_type') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.farmer_type_helper') }}
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col {{ $errors->has('gender') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.gender') }}
                            <div class="d-flex form-control col  mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="male" type="radio" name="gender">
                                    <label class="form-check-label" for="male">Male</label>
                                    @if($errors->has('gender'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('gender') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.gender_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="female" type="radio" name="gender">
                                    <label class="form-check-label" for="female">Female</label>
                                    @if($errors->has('gender'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('gender') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.gender_helper') }}
                                    </p>
                                </div>

                            </div>
                        </div>
                        <div class="form-group col {{ $errors->has('trainee_required') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.trainee_required') }}
                            <div class="d-flex form-control col  mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="yes_trainee" type="radio"
                                        name="trainee_required">
                                    <label class="form-check-label" for="yes_trainee">Yes</label>
                                    @if($errors->has('trainee_required'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('trainee_required') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.trainee_required_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="no_trainee" type="radio"
                                        name="trainee_required">
                                    <label class="form-check-label" for="no_trainee">No</label>
                                    @if($errors->has('trainee_required'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('trainee_required') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.trainee_required_helper') }}
                                    </p>
                                </div>

                            </div>

                        </div>
                        <div class="form-group col {{ $errors->has('bank_account') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.bank_account') }}
                            <div class="d-flex form-control col  mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="yes_bank_account" type="radio"
                                        name="bank_account">
                                    <label class="form-check-label" for="yes_bank_account">Yes</label>
                                    @if($errors->has('bank_account'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('bank_account') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.bank_account_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="no_bank_account" type="radio"
                                        name="bank_account">
                                    <label class="form-check-label" for="no_bank_account">No</label>
                                    @if($errors->has('bank_account'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('bank_account') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.bank_account_helper') }}
                                    </p>
                                </div>

                            </div>

                        </div>
                        <div class="form-group col {{ $errors->has('kisan_credit_card') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.kisan_credit_card') }}
                            <div class="d-flex form-control col  mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="yes_kisan_credit_card" type="radio"
                                        name="kisan_credit_card">
                                    <label class="form-check-label" for="yes_kisan_credit_card">Yes</label>
                                    @if($errors->has('kisan_credit_card'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('kisan_credit_card') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.kisan_credit_card_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="no_kisan_credit_card" type="radio"
                                        name="kisan_credit_card">
                                    <label class="form-check-label" for="no_kisan_credit_card">No</label>
                                    @if($errors->has('kisan_credit_card'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('kisan_credit_card') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.kisan_credit_card_helper') }}
                                    </p>
                                </div>

                            </div>

                        </div>
                        <div class="form-group col {{ $errors->has('loan') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.loan') }}
                            <div class="d-flex form-control col  mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="yes_loan" type="radio" name="loan">
                                    <label class="form-check-label" for="yes_loan">Yes</label>
                                    @if($errors->has('loan'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('loan') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.loan_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="no_loan" type="radio" name="loan">
                                    <label class="form-check-label" for="no_loan">No</label>
                                    @if($errors->has('loan'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('loan') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.loan_helper') }}
                                    </p>
                                </div>

                            </div>

                        </div>
                        <div class="form-group col {{ $errors->has('shg') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.shg') }}
                            <div class="d-flex form-control col  mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="yes_shg" type="radio" name="shg">
                                    <label class="form-check-label" for="yes_shg">Yes</label>
                                    @if($errors->has('shg'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('shg') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.shg_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="no_shg" type="radio" name="shg">
                                    <label class="form-check-label" for="no_shg">No</label>
                                    @if($errors->has('shg'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('shg') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.shg_helper') }}
                                    </p>
                                </div>

                            </div>

                        </div>
                        <div class="form-group col {{ $errors->has('informal_loan') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.informal_loan') }}
                            <div class="d-flex form-control col  mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="yes_informal_loan" type="radio"
                                        name="informal_loan">
                                    <label class="form-check-label" for="yes_informal_loan">Yes</label>
                                    @if($errors->has('informal_loan'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('informal_loan') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.informal_loan_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="no_informal_loan" type="radio"
                                        name="informal_loan">
                                    <label class="form-check-label" for="no_informal_loan">No</label>
                                    @if($errors->has('informal_loan'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('informal_loan') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.informal_loan_helper') }}
                                    </p>
                                </div>

                            </div>

                        </div>
                        <div class="form-group col {{ $errors->has('marital_status') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.marital_status') }}
                            <div class="d-flex form-control col  mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="yes_marital_status" type="radio"
                                        name="marital_status">
                                    <label class="form-check-label" for="yes_marital_status">Yes</label>
                                    @if($errors->has('marital_status'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('marital_status') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.marital_status_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="no_marital_status" type="radio"
                                        name="marital_status">
                                    <label class="form-check-label" for="no_marital_status">No</label>
                                    @if($errors->has('marital_status'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('marital_status') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.marital_status_helper') }}
                                    </p>
                                </div>

                            </div>

                        </div>
                        <div class="form-group col {{ $errors->has('literate') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.literate') }}
                            <div class="d-flex form-control col  mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="yes_literate" type="radio" name="literate">
                                    <label class="form-check-label" for="yes_literate">Yes</label>
                                    @if($errors->has('literate'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('literate') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.literate_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="no_literate" type="radio" name="literate">
                                    <label class="form-check-label" for="no_literate">No</label>
                                    @if($errors->has('literate'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('literate') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.literate_helper') }}
                                    </p>
                                </div>

                            </div>

                        </div>
                        <div class="form-group col {{ $errors->has('organic_farming') ? 'has-error' : '' }}">
                            {{ trans('cruds.farmer.fields.organic_farming') }}
                            <div class="d-flex form-control col  mt-2 p-2 ">
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="yes" type="radio" name="organic_farming">
                                    <label class="form-check-label" for="yes">Yes</label>
                                    @if($errors->has('organic_farming'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('organic_farming') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.organic_farming_helper') }}
                                    </p>
                                </div>
                                <div class="form-check pr-2 d-flex">
                                    <input class="form-check-input" id="no" type="radio" name="organic_farming">
                                    <label class="form-check-label" for="no">No</label>
                                    @if($errors->has('organic_farming'))
                                    <em class="invalid-feedback">
                                        {{ $errors->first('organic_farming') }}
                                    </em>
                                    @endif
                                    <p class="helper-block">
                                        {{ trans('cruds.farmer.fields.organic_farming_helper') }}
                                    </p>
                                </div>

                            </div>

                        </div>
                    </div>
                    <div class="row  border rounded  mt-2  p-2 ml-2">
                        <div class="row m-1 p-3 border rounded">
                            <div class="row  col">
                                <label for="monsoon" class="btn btn-xs btn-primary button p-2 w-50 h-55">{{
                                    trans('cruds.farmer.fields.monsoon') }}</label>
                            </div>
                            <div class="form-group col-sm-3{{ $errors->has('crop_name') ? 'has-error' : '' }}">
                                <label for="crop_name">{{ trans('cruds.farmer.fields.crop_name') }}*</label>
                                <input type="text" id="crop_name" name="crop_name" class="form-control" required>
                                @if($errors->has('crop_name'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('crop_name') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.crop_name_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-2{{ $errors->has('area') ? 'has-error' : '' }}">
                                <label for="area">{{ trans('cruds.farmer.fields.area') }}*</label>
                                <input type="text" id="area" name="area" class="form-control" required>
                                @if($errors->has('area'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('area') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.area_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3{{ $errors->has('total_production') ? 'has-error' : '' }}">
                                <label for="total_production">{{ trans('cruds.farmer.fields.total_production')
                                    }}*</label>
                                <input type="text" id="total_production" name="total_production" class="form-control"
                                    required>
                                @if($errors->has('total_production'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('total_production') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.total_production_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-4{{ $errors->has('average_yield') ? 'has-error' : '' }}">
                                <label for="average_yield">{{ trans('cruds.farmer.fields.average_yield') }}*</label>
                                <input type="text" id="average_yield" name="average_yield" class="form-control"
                                    value="{{ old('average_yield', isset($average_yield) ? $average_yield->average_yield : '') }}"
                                    required>
                                @if($errors->has('average_yield'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('average_yield') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.average_yield_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-2{{ $errors->has('direct') ? 'has-error' : '' }}">
                                <label for="direct">{{ trans('cruds.farmer.fields.direct') }}*</label>
                                <input type="text" id="direct" name="direct" class="form-control" required>
                                @if($errors->has('direct'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('direct') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.direct_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-4{{ $errors->has('through_intermediaries') ? 'has-error' : '' }}">
                                <label for="through_intermediaries">{{
                                    trans('cruds.farmer.fields.through_intermediaries') }}*</label>
                                <input type="text" id="through_intermediaries" name="through_intermediaries"
                                    class="form-control" required>
                                @if($errors->has('through_intermediaries'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('through_intermediaries') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.through_intermediaries_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-2{{ $errors->has('total') ? 'has-error' : '' }}">
                                <label for="total">{{ trans('cruds.farmer.fields.total') }}*</label>
                                <input type="text" id="total" name="total" class="form-control" required>
                                @if($errors->has('total'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('total') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.total_helper') }}
                                </p>
                            </div>
                        </div>
                        <div class="row m-1 p-3 border rounded">
                            <div class="row form-group col">
                                <label for="winter" class="btn btn-xs btn-primary button p-2 w-50 h-55">{{
                                    trans('cruds.farmer.fields.winter') }}</label>
                            </div>
                            <div class="form-group col-sm-3{{ $errors->has('crop_name') ? 'has-error' : '' }}">
                                <label for="crop_name">{{ trans('cruds.farmer.fields.crop_name') }}*</label>
                                <input type="text" id="crop_name" name="crop_name" class="form-control" required>
                                @if($errors->has('crop_name'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('crop_name') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.crop_name_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-2{{ $errors->has('area') ? 'has-error' : '' }}">
                                <label for="area">{{ trans('cruds.farmer.fields.area') }}*</label>
                                <input type="text" id="area" name="area" class="form-control" required>
                                @if($errors->has('area'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('area') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.area_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3{{ $errors->has('total_production') ? 'has-error' : '' }}">
                                <label for="total_production">{{ trans('cruds.farmer.fields.total_production')
                                    }}*</label>
                                <input type="text" id="total_production" name="total_production" class="form-control"
                                    required>
                                @if($errors->has('total_production'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('total_production') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.total_production_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-4{{ $errors->has('average_yield') ? 'has-error' : '' }}">
                                <label for="average_yield">{{ trans('cruds.farmer.fields.average_yield') }}*</label>
                                <input type="text" id="average_yield" name="average_yield" class="form-control"
                                    value="{{ old('average_yield', isset($average_yield) ? $average_yield->average_yield : '') }}"
                                    required>
                                @if($errors->has('average_yield'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('average_yield') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.average_yield_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-2{{ $errors->has('direct') ? 'has-error' : '' }}">
                                <label for="direct">{{ trans('cruds.farmer.fields.direct') }}*</label>
                                <input type="text" id="direct" name="direct" class="form-control" required>
                                @if($errors->has('direct'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('direct') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.direct_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-4{{ $errors->has('through_intermediaries') ? 'has-error' : '' }}">
                                <label for="through_intermediaries">{{
                                    trans('cruds.farmer.fields.through_intermediaries') }}*</label>
                                <input type="text" id="through_intermediaries" name="through_intermediaries"
                                    class="form-control" required>
                                @if($errors->has('through_intermediaries'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('through_intermediaries') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.through_intermediaries_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-2{{ $errors->has('total') ? 'has-error' : '' }}">
                                <label for="total">{{ trans('cruds.farmer.fields.total') }}*</label>
                                <input type="text" id="total" name="total" class="form-control" required>
                                @if($errors->has('total'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('total') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.total_helper') }}
                                </p>
                            </div>
                        </div>
                        <div class="row m-1 p-3 border rounded">
                            <div class="row form-group col">
                                <label for="summer" class="btn btn-xs btn-primary button p-2 w-50 h-55 ">{{
                                    trans('cruds.farmer.fields.summer') }}</label>
                            </div>
                            <div class="form-group col-sm-3{{ $errors->has('crop_name') ? 'has-error' : '' }}">
                                <label for="crop_name">{{ trans('cruds.farmer.fields.crop_name') }}*</label>
                                <input type="text" id="crop_name" name="crop_name" class="form-control" required>
                                @if($errors->has('crop_name'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('crop_name') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.crop_name_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-2{{ $errors->has('area') ? 'has-error' : '' }}">
                                <label for="area">{{ trans('cruds.farmer.fields.area') }}*</label>
                                <input type="text" id="area" name="area" class="form-control" required>
                                @if($errors->has('area'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('area') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.area_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-3{{ $errors->has('total_production') ? 'has-error' : '' }}">
                                <label for="total_production">{{ trans('cruds.farmer.fields.total_production')
                                    }}*</label>
                                <input type="text" id="total_production" name="total_production" class="form-control"
                                    required>
                                @if($errors->has('total_production'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('total_production') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.total_production_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-4{{ $errors->has('average_yield') ? 'has-error' : '' }}">
                                <label for="average_yield">{{ trans('cruds.farmer.fields.average_yield') }}*</label>
                                <input type="text" id="average_yield" name="average_yield" class="form-control"
                                    value="{{ old('average_yield', isset($average_yield) ? $average_yield->average_yield : '') }}"
                                    required>
                                @if($errors->has('average_yield'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('average_yield') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.average_yield_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-2{{ $errors->has('direct') ? 'has-error' : '' }}">
                                <label for="direct">{{ trans('cruds.farmer.fields.direct') }}*</label>
                                <input type="text" id="direct" name="direct" class="form-control" required>
                                @if($errors->has('direct'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('direct') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.direct_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-4{{ $errors->has('through_intermediaries') ? 'has-error' : '' }}">
                                <label for="through_intermediaries">{{
                                    trans('cruds.farmer.fields.through_intermediaries') }}*</label>
                                <input type="text" id="through_intermediaries" name="through_intermediaries"
                                    class="form-control" required>
                                @if($errors->has('through_intermediaries'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('through_intermediaries') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.through_intermediaries_helper') }}
                                </p>
                            </div>
                            <div class="form-group col-sm-2{{ $errors->has('total') ? 'has-error' : '' }}">
                                <label for="total">{{ trans('cruds.farmer.fields.total') }}*</label>
                                <input type="text" id="total" name="total" class="form-control" required>
                                @if($errors->has('total'))
                                <em class="invalid-feedback">
                                    {{ $errors->first('total') }}
                                </em>
                                @endif
                                <p class="helper-block">
                                    {{ trans('cruds.farmer.fields.total_helper') }}
                                </p>
                            </div>
                        </div>
                       
                    </div>


                </div>
            </div>  
            <div class="mt-2">
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </div>
    </form>


</div>
</div>
@endsection