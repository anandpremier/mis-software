@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.fpo.title_singular') }}
    </div>

    <div class="card-body">
        <form action="" method="POST" enctype="multipart/form-data">
            @csrf
        <div class="row">
            <div class="form-group col{{ $errors->has('fpo_name') ? 'has-error' : '' }}">
                <label for="fpo_name">{{ trans('cruds.fpo.fields.fpo_name') }}*</label>
                <input type="text" id="fpo_name" name="fpo_name" class="form-control"
                    value="{{ old('fpo_name', isset($fpo) ? $fpo->fpo_name : '') }}" required>
                @if($errors->has('fpo_name'))
                <em class="invalid-feedback">
                    {{ $errors->first('fpo_name') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.fpo_name_helper') }}
                </p>
            </div>
            <div class="form-group col{{ $errors->has('address') ? 'has-error' : '' }}">
                <label for="address">{{ trans('cruds.fpo.fields.address') }}*</label>
                <input type="address" id="address" name="address" class="form-control"
                    value="{{ old('address', isset($fpo) ? $fpo->address : '') }}" required>
                @if($errors->has('address'))
                <em class="invalid-feedback">
                    {{ $errors->first('address') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.address_helper') }}
                </p>
            </div>
        </div>
        <div class="row">
            <div class="form-group col {{ $errors->has('taluka') ? 'has-error' : '' }}">
                <label for="taluka">{{ trans('cruds.fpo.fields.taluka') }}*</label>
                <input type="taluka" id="taluka" name="taluka" class="form-control" required>
                @if($errors->has('taluka'))
                <em class="invalid-feedback">
                    {{ $errors->first('taluka') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.taluka_helper') }}
                </p>
            </div>
            <div class="form-group col {{ $errors->has('district') ? 'has-error' : '' }}">
                <label for="district">{{ trans('cruds.fpo.fields.district') }}*</label>
                <input type="district" id="district" name="district" class="form-control" required>
                @if($errors->has('district'))
                <em class="invalid-feedback">
                    {{ $errors->first('district') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.district_helper') }}
                </p>
            </div>
        </div>
        <div class="row">
            <div class="form-group col {{ $errors->has('state') ? 'has-error' : '' }}">
                <label for="state">{{ trans('cruds.fpo.fields.state') }}*</label>
                <input type="state" id="state" name="state" class="form-control" required>
                @if($errors->has('state'))
                <em class="invalid-feedback">
                    {{ $errors->first('state') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.state_helper') }}
                </p>
            </div>
            <div class="form-group col {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email">{{ trans('cruds.fpo.fields.email') }}*</label>
                <input type="email" id="email" name="email" class="form-control" required>
                @if($errors->has('email'))
                <em class="invalid-feedback">
                    {{ $errors->first('email') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.email_helper') }}
                </p>
            </div>
        </div>
        <div class="row">
            <div class="form-group col {{ $errors->has('contact_person') ? 'has-error' : '' }}">
                <label for="contact_person">{{ trans('cruds.fpo.fields.contact_person') }}*</label>
                <input type="contact_person" id="contact_person" name="contact_person" class="form-control" required>
                @if($errors->has('contact_person'))
                <em class="invalid-feedback">
                    {{ $errors->first('contact_person') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.contact_person_helper') }}
                </p>
            </div>
            <div class="form-group col {{ $errors->has('mobile_no') ? 'has-error' : '' }}">
                <label for="mobile_no">{{ trans('cruds.fpo.fields.mobile_no') }}*</label>
                <input type="mobile_no" id="mobile_no" name="mobile_no" class="form-control" required>
                @if($errors->has('mobile_no'))
                <em class="invalid-feedback">
                    {{ $errors->first('mobile_no') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.mobile_no_helper') }}
                </p>
            </div>
        </div>
            <div class="form-group row align-items-end{{ $errors->has('gst_no') ? 'has-error' : '' }}">
                <div class="col">
                <label for="gst_no">{{ trans('cruds.fpo.fields.gst_no') }}*</label>
                <input type="gst_no" id="gst_no" name="gst_no" class="form-control" required>
                @if($errors->has('gst_no'))
                <em class="invalid-feedback">
                    {{ $errors->first('gst_no') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.gst_no_helper') }}
                </p>
            </div>
                <div class="input-group mb-3 col">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="gstInputFile">
                        <label class="custom-file-label" for="gstInputFile">Choose
                            file</label>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                    </div>
                </div>
            </div>
            <div class="form-group  row align-items-end {{ $errors->has('tan_no') ? 'has-error' : '' }}">
                <div class="col">
                <label for="tan_no">{{ trans('cruds.fpo.fields.tan_no') }}*</label>
                <input type="tan_no" id="tan_no" name="tan_no" class="form-control" required>
                @if($errors->has('tan_no'))
                <em class="invalid-feedback">
                    {{ $errors->first('tan_no') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.tan_no_helper') }}
                </p>
            </div>
                <div class="input-group mb-3 col ">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="tanInputFile">
                        <label class="custom-file-label" for="tanInputFile">Choose
                            file</label>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                    </div>
                </div>
            </div>
            <div class="form-group  row align-items-end {{ $errors->has('pan_no') ? 'has-error' : '' }}">
               <div class="col">
                <label for="pan_no">{{ trans('cruds.fpo.fields.pan_no') }}*</label>
                <input type="pan_no" id="pan_no" name="pan_no" class="form-control" required>
                @if($errors->has('pan_no'))
                <em class="invalid-feedback">
                    {{ $errors->first('pan_no') }}
                </em>
                @endif
                <p class="helper-block">
                    {{ trans('cruds.fpo.fields.pan_no_helper') }}
                </p>
            </div>
                <div class="input-group mb-3 col ">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="panInputFile">
                        <label class="custom-file-label" for="panInputFile">Choose
                            file</label>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                    </div>
                </div>
            </div>
            <div class="form-group row align-items-end {{ $errors->has('cin_no') ? 'has-error' : '' }}">
                <div class=" col">
                    <label for="cin_no">{{ trans('cruds.fpo.fields.cin_no') }}*</label>
                    <input type="cin_no" id="cin_no" name="cin_no" class="form-control" required>
                    @if($errors->has('cin_no'))
                    <em class="invalid-feedback">
                        {{ $errors->first('cin_no') }}
                    </em>
                    @endif
                    <p class="helper-block">
                        {{ trans('cruds.fpo.fields.cin_no_helper') }}
                    </p>
                </div>
                <div class="input-group mb-3 col">
                    <div class="custom-file">
                        <input type="file" class="custom-file-input" id="cinInputFile">
                        <label class="custom-file-label" for="cinInputFile">Choose
                            file</label>
                    </div>
                    <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                    </div>
                </div>
            </div>







            <div>
                <input class="btn btn-danger" type="submit" value="{{ trans('global.save') }}">
            </div>
        </form>


    </div>
</div>
@endsection